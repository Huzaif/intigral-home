$(document).ready(function() {
 $(".suckerfish").hover(
  function(){
   $(this).find('a').addClass('new');
   $("div.dropdown",this).css({display:'block'})
  }, 
  function(){
   $(this).find('a').removeClass('new');
   $("div.dropdown",this).css({display:'none'});
  }
 );
 $('.open-hamburger').click(function () {
  $('body').addClass('nav-open');
 });
 $('.close').click(function () {
  $('body').removeClass('nav-open');
 });
 $('#fullpage').fullpage({
  'navigation': true,
  anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage'],
  menu: '#nav',
  responsiveHeight: 740,
  'navigationPosition': 'left',
  slidesNavigation: true,
  animateAnchor:false,
  css3: true,
  animateAnchor: true,
  afterRender: function(){
   $('#fp-nav').find('li').each(function(i){
    $(this).find('a span').remove();
    $(this).find('a').text('0'+parseInt(i+1));
   });
  },
//  'afterSlideLoad': function(anchorLink, index, slideAnchor, slideIndex){
//   
//   if(index == 0 && slideIndex == 1){
//    $('#dawri-ipad, #dawri-iphone').addClass('active');
//   }
//  },
 });
});