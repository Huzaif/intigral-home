'use strict';

/*Gulp Packages*/
var gulp  = require('gulp');
var gutil = require('gulp-util');

/*Compile CSS  with Advance Error Reporting*/
var path = require('path');

/*Compile Compass files*/
var compass = require('gulp-compass');

/*Add Prefix to your CSS Output*/
var autoprefixer = require('gulp-autoprefixer');

/*Sync Changes to Browser*/    
var browserSync = require('browser-sync').create();
var htmlInjector = require("bs-html-injector");

/*Show Debug Msg so we know whats going On*/
var debug = require('gulp-debug');

/*Cache content to optimize performance*/
var cache = require('gulp-cached');

/*Only pass through changed files*/
var changed = require('gulp-changed');

/*Prevent pipe breaking caused by errors from gulp plugins*/
var plumber = require('gulp-plumber');

/*Load Gulp Swiq For Template Management*/
var swig = require('gulp-swig');
/***********************************************************************************************/
/***********************************************************************************************/

/*Define the default Gulp task*/
gulp.task('default', ['watch','browser-sync']);


/*Compile HTML file with file include module and write compiled HTML on Dist*/
gulp.task('build-html', function(){
    return gulp.src(['./src/*.html'])
        .pipe(swig({defaults: { cache: false }}))
        .pipe(changed('./dist', {hasChanged: changed.compareSha1Digest}))
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream())
        .pipe(debug());
});


/*Compile CSS Files*/
gulp.task('build-css', function () {
 return gulp.src(['./src/sass/style.scss'])
  .pipe(plumber())
  .pipe(compass({
  config_file: './config.rb',
  css: 'src/css',
  sass: 'src/sass'
 }))
  .on('error', function(error) {
  // Would like to catch the error here
  console.log(error);
  this.emit('end');
 })
  .pipe(autoprefixer({
  browsers: ['last 4 versions'],
  cascade: false
 }))
  .pipe(changed('./dist/css', {hasChanged: changed.compareSha1Digest}))
  .pipe(gulp.dest('./dist/css'))  
  .pipe(browserSync.stream())
  .pipe(debug());
});

/*Simple update Js files from src to dist*/
gulp.task('build-js', function(done){
    return gulp.src(['./src/js/**/*.*'])
        .pipe(changed('./dist/js'))
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream())
        .pipe(debug());
});

/*Optimize Images and updated on dist*/
gulp.task('build-img', function(){
    return gulp.src(['./src/images/**/*.*'])
        .pipe(changed('./dist/images'))
        .pipe(gulp.dest('./dist/images'))
        .pipe(browserSync.stream())
        .pipe(debug());
});


/*Browser Sync Task*/
gulp.task('browser-sync', function() {
    
/*    browserSync.use(htmlInjector, {
        // Files to watch that will trigger the injection 
        files: "./dist/!*.html"
    });*/
    
    browserSync.init({
        server: {
            baseDir: "./dist",
            directory: true
        },
        
        ui: {
            weinre: {
                    port: 3002
            }
        }
                     
    });
     
});


/* Configure which files to watch and what tasks to use on file changes*/
gulp.task('watch', function() {
 gulp.watch('./src/js/**/*.*', ['build-js']);
 gulp.watch('./src/sass/**/*.scss', ['build-css']);
 gulp.watch('./src/*.html', ['build-html']);
 gulp.watch('./src/templates/*.*', ['build-html']);
 gulp.watch('./src/img/**/*.*', ['build-img']);
});